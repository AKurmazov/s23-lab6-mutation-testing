import pytest

from .bonus_system import calculateBonuses

ALLOWED_ERROR = 1e-16


@pytest.mark.parametrize(
    ('program', 'amount', 'expected_bonus',),
    [
        ('Standard', 0, 0.5),
        ('Standard', 1, 0.5),
        ('Standard', 10000, 0.75),
        ('Standard', 10001, 0.75),
        ('Standard', 50000, 1),
        ('Standard', 50001, 1),
        ('Standard', 100000, 1.25),
        ('Standard', 100001, 1.25),
        ('Premium', 0, 0.1),
        ('Premium', 1, 0.1),
        ('Premium', 10000, 0.15),
        ('Premium', 10001, 0.15),
        ('Premium', 50000, 0.2),
        ('Premium', 50001, 0.2),
        ('Premium', 100000, 0.25),
        ('Premium', 100001, 0.25),
        ('Diamond', 0, 0.2),
        ('Diamond', 1, 0.2),
        ('Diamond', 10000, 0.3),
        ('Diamond', 10001, 0.3),
        ('Diamond', 50000, 0.4),
        ('Diamond', 50001, 0.4),
        ('Diamond', 100000, 0.5),
        ('Diamond', 100001, 0.5),
        ('Invalid', 0, 0),
        ('    ', 1, 0),
        ('Invalid', 10000, 0),
        ('AAAA', 10001, 0),
        ('Invalid', 50000, 0),
        ('XXXX', 50001, 0),
        ('Invalid', 100000, 0),
        ('ZZZZ', 100001, 0),
    ],
)
def test_calculate_bonuses(program, amount, expected_bonus):
    assert abs(calculateBonuses(program, amount) - expected_bonus) <= ALLOWED_ERROR
